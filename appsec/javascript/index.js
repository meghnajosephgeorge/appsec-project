var express = require('express');

var app = express();

app.get('/', function (req, res) {
 res.send('I Love JS Containers!');
});

app.listen(3000, function () {
 console.log('Examples app listening on port 3000!');
});
