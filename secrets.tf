#Using example from https://blog.gruntwork.io/a-comprehensive-guide-to-managing-secrets-in-your-terraform-code-1d586955ace1
# Key located in account tuid-tups-wr-whared
# export AWS_KMS_KEY="d053...8463" 
# aws kms encrypt --plaintext fileb://.secrets/ngpba_agent_creds.dev.json --key-id $AWS_KMS_KEY --output text --query CiphertextBlob > .secrets/ngpba_agent_creds.dev.json.encrypted
# aws kms decrypt --ciphertext-blob file://.secrets/ngpba_agent_creds.dev.json.encrypted --key-id $AWS_KMS_KEY --output text --query Plaintext | base64 --decode

###################
# tuid-dev-secret-tupspnl-app-db
data "aws_kms_secrets" "creds" {
  secret {
    name    = "db"
    payload = file("${path.module}/db-creds.yml.encrypted")
  }
}

locals {
  db_creds = jsonencode(data.aws_kms_secrets.creds.plaintext["db"])
}